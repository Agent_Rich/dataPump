import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm") version "1.8.22"
    kotlin("plugin.noarg") version "1.9.10"
}

noArg {
    annotation("com.rich.datapump.NoArg")
    invokeInitializers = true
}

repositories {
    maven { url = uri("https://mirrors.cloud.tencent.com/nexus/repository/maven-public/") }
    mavenLocal()
    mavenCentral()
}

group = "com.rich"
version = "1.0.1"


dependencies {
    implementation(platform("org.noear:solon-parent:2.6.6"))
    
    implementation("org.noear:solon-api")
    implementation("org.noear:solon.logging.simple")

    implementation("org.noear:mybatis-flex-solon-plugin")
    implementation("com.mybatis-flex:mybatis-flex-kotlin-extensions:1.0.5")
    implementation("com.zaxxer:HikariCP:5.0.1")
    implementation ("com.oracle.database.jdbc:ojdbc8:23.2.0.0")
    implementation ("com.oracle.database.nls:orai18n:23.2.0.0")
    implementation("com.mysql:mysql-connector-j:8.2.0")
    implementation ("com.alibaba:easyexcel:3.3.2")
    implementation ("com.ibeetl:beetl:3.15.10.RELEASE")
    implementation ("org.noear:solon.boot.socketd")
//    implementation ("org.noear:socketd-transport-java-tcp")
    implementation ("org.noear:socketd-transport-java-websocket:2.3.8")
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-parameters")
}
tasks.withType<KotlinCompile> {
    kotlinOptions.javaParameters = true
}

tasks.withType<Jar> {
    manifest {
        attributes.apply {
            set("Main-Class", "com.rich.datapump.AppKt")
        }
    }

    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    from(configurations.runtimeClasspath.get().map {
        if (it.isDirectory) it else zipTree(it)
    })

    from(sourceSets.main.get().output)
}