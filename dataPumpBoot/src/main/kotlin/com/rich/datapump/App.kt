package com.rich.datapump

import org.noear.solon.Solon
import org.noear.solon.annotation.SolonMain

@SolonMain
class App

fun main(args: Array<String>) {
    Solon.start(App::class.java, args){app->
        //启用 Sokcet.D 服务（它是 solon.boot.socketd 插件的启用控制）
        app.enableSocketD(true)
    }
}