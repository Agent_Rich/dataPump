package com.rich.datapump.biz.template

import com.rich.datapump.biz.metadata.MetadataBean

data class TRDataBean (
    var destPath: String? = null,
    var templateRootPath: String? = null,
    var metadatas: List<MetadataBean>? = null
)