package com.rich.datapump.biz.datasource

import com.mybatisflex.core.datasource.DataSourceKey
import org.noear.solon.annotation.Component
import org.noear.solon.annotation.Inject


/**
 * 负责管理和提供当前使用的数据源以及对数据库操作的类
 */
@Component
class CurrDSHandler {
    @Inject
    lateinit var mysqlDSMapper: MysqlDSMapper
    @Inject
    lateinit var oracleDSMapper: OracleDSMapper

     var usingDBInfo = DBInfo.falseValue()
        set(value) {
            field = value
            DataSourceKey.use(value.name)
        }

    fun getCurrDSMapper(): CurrDSMapper =
        when(usingDBInfo.type){
                "mysql" ->
                    mysqlDSMapper
                "oracle" ->
                    oracleDSMapper
                else -> throw IllegalArgumentException("不支持的类型${usingDBInfo.type}")
        }


}