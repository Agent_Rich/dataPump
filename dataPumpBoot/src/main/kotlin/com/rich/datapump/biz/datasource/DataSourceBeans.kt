package com.rich.datapump.biz.datasource

import com.rich.datapump.NoArg

@NoArg
data class DBInfo(
    var name: String,
    var url: String,
    var username: String,
    var password: String,
    var type: String
) {
    fun driverClassName(): String =
        when (type) {
            "mysql" -> "com.mysql.cj.jdbc.Driver"
            "postgre" -> "org.postgresql.Driver"
            "oracle" -> "oracle.jdbc.driver.OracleDriver"
            "sqlserver" -> "com.microsoft.sqlserver.jdbc.SQLServerDriver"
            else -> throw IllegalArgumentException("不支持数据库类型: $type")
        }

    companion object{
        /**
         * 假值  用于占位使用
         */
        fun falseValue() = DBInfo("","","","","")
    }
}