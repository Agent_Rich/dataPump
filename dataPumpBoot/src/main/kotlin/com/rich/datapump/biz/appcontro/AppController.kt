package com.rich.datapump.biz.appcontro

import org.noear.solon.Solon
import org.noear.solon.annotation.Component
import org.noear.solon.annotation.Controller
import org.noear.solon.annotation.Mapping
import org.noear.solon.annotation.Socket
import org.noear.solon.core.event.AppLoadEndEvent
import org.noear.solon.core.event.EventListener
import kotlin.system.exitProcess

@Controller
@Mapping("app")
@Component
class AppController : EventListener<AppLoadEndEvent> {
    @Mapping("exit")
    fun exit(){
        Solon.stop()
    }

    override fun onEvent(event: AppLoadEndEvent?) {
        Runtime.getRuntime().exec("cmd   /c   start http://localhost:11431/dataPump-api/index.html ")
    }


}