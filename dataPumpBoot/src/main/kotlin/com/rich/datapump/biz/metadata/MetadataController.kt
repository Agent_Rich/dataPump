package com.rich.datapump.biz.metadata

import com.rich.datapump.biz.datasource.CurrDSHandler
import org.noear.solon.annotation.Controller
import org.noear.solon.annotation.Inject
import org.noear.solon.annotation.Mapping

@Controller
@Mapping("metadata")
class MetadataController {

    @Inject
    lateinit var dsHandler: CurrDSHandler

    /**
     * 获取元数据列表
     */
    @Mapping("MDList")
    fun mdList() = dsHandler.getCurrDSMapper().getMetadatas()
}