package com.rich.datapump.biz.metadata

import com.mybatisflex.annotation.Column

data class MetadataBean(
    @Column("TABLE_NAME")
    var tableName: String? = null,
    @Column("TABLE_COMMENT")
    var tableComment: String? = null,

    var namePascal: String? = null,
    var nameHump: String? = null,
    var packagePath: String? = null,
    var columns: List<com.rich.datapump.biz.metadata.Column>? = null
)

data class Column(
    @Column("COLUMN_NAME")
    var columnName: String? = null,
    @Column("DATA_TYPE")
    var dataType: String? = null,
    @Column("COLUMN_COMMENT")
    var columnComment: String? = null,
    @Column("DATALENGTH")
    var dataLength: Long? = null,
    var namePascal: String? = null,
    var nameHump: String? = null,
    var ifID: Boolean = false,
    var ifInBaseModel:Boolean = false
)