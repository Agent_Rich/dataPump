package com.rich.datapump.filters

import com.mybatisflex.core.datasource.DataSourceKey
import com.rich.datapump.biz.datasource.CurrDSHandler
import org.noear.solon.annotation.Component
import org.noear.solon.annotation.Inject
import org.noear.solon.core.handle.Context
import org.noear.solon.core.handle.Filter
import org.noear.solon.core.handle.FilterChain
import org.slf4j.LoggerFactory

@Component
class DynamicDBFilter:Filter {
    private val logger = LoggerFactory.getLogger(DynamicDBFilter::class.java)

    @Inject
    lateinit var dsHandler: CurrDSHandler

    override fun doFilter(ctx: Context, chain: FilterChain) {

        val dbKey = dsHandler.usingDBInfo.name
        if(dbKey.isNotBlank())
            DataSourceKey.use(dbKey)
        chain.doFilter(ctx)
    }

}