package com.rich.datapump.config.beetlExt

import org.beetl.core.Context
import org.beetl.core.Function;
import java.io.IOException


class TypePrint4Java : Function {
    override fun call(paras: Array<Any?>, ctx: Context): Any {
        val o = paras[0]
        if (o != null) {
            try {
                var typeStr = ""
                typeStr = when (o as String?) {
                    "VARCHAR2", "varchar" -> "String"
                    "DATE", "datetime" -> "LocalDateTime"
                    "int", "NUMBER" -> "Integer"
                    "decimal" -> "Double"
                    else -> ""
                }
                ctx.byteWriter.write(typeStr.toCharArray())
            } catch (e: IOException) {
                throw RuntimeException(e)
            }
        }
        return ""
    }
}

class TypePrint4TS : Function {
    override fun call(paras: Array<Any>, ctx: Context): Any {
        val o = paras[0]
        if (o != null) {
            try {
                var typeStr = ""
                typeStr = when (o as String) {
                    "VARCHAR2", "varchar", "DATE", "datetime" -> "string"
                    "int", "NUMBER", "decimal" -> "number"
                    else -> ""
                }
                ctx.byteWriter.write(typeStr.toCharArray())
            } catch (e: IOException) {
                throw java.lang.RuntimeException(e)
            }
        }
        return ""
    }
}
