package com.rich.datapump.config

import org.noear.solon.net.annotation.ServerEndpoint
import org.noear.solon.net.socketd.handle.ToHandlerListener

@ServerEndpoint("dataPump-api/mvc")
class SocketdAsMvc: ToHandlerListener() {
}