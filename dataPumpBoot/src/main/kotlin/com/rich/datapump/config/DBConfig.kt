package com.rich.datapump.config

import com.zaxxer.hikari.HikariDataSource
import org.noear.solon.annotation.Bean
import org.noear.solon.annotation.Configuration
import org.noear.solon.annotation.Inject
import javax.sql.DataSource

@Configuration
class DBConfig {
    @Bean(name = "db1", typed = true)
    fun db1(@Inject("\${demo.db1}") ds: HikariDataSource): DataSource {
        return ds
    }
}