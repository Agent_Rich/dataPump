import { createApp } from 'vue'
import ArcoVue from '@arco-design/web-vue';
import App from './App.vue';
import '@arco-design/web-vue/dist/arco.css';
// import { Message } from '@arco-design/web-vue';

import './assets/main.css'

// 额外引入图标库
import ArcoVueIcon from '@arco-design/web-vue/es/icon';
import VueClipboard from 'vue-clipboard2'
import { createPinia } from 'pinia'


const pinia = createPinia()

const app = createApp(App);
app.use(ArcoVue);
app.use(ArcoVueIcon);
app.use(VueClipboard)
app.use(pinia)
// Message._context = app._context;
app.mount('#app');


