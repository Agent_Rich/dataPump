import axios from 'axios'
import { Message } from '@arco-design/web-vue';

let base = '/dataPump-api'
const service = axios.create({
    timeout: 60000, // 请求超时
    headers: {
      "Content-Type": "application/json; charset=UTF-8;"
    }
  });


  service.interceptors.response.use(response => {
    let ret = response.data
    console.log('ret',Message)
    // if (ret.code && ret.code === "200" ) {
    //     if(ret.msg)
    //     Message.success(ret.msg)
    // }else {
    //     Message.error(ret.msg)
        // return Promise.reject(ret);
    // }
    return Promise.resolve(ret) ;
}, error => {
    if (error.response.status == 504 || error.response.status == 404) {
        Message.error('服务器被吃了( ╯□╰ )')
    } else if (error.response.status == 403) {
        Message.error('权限不足，请联系管理员')
    } else if (error.response.status == 401) {
        Message.error('尚未登录，请登录')
    } else {
        if (error.response.data.msg) {
            Message.error(error.response.data.msg)
        } else {
            Message.error('未知错误!')
        }
    }
    return;
})



export const postKeyValueRequest = (url, params) => {
    return service.post({
        url: `${base}${url}`,
        data: params,
        transformRequest: [function (data) {
            let ret = '';
            for (let i in data) {
                ret += encodeURIComponent(i) + '=' + encodeURIComponent(data[i]) + '&'
            }
            return ret;
        }],
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
    
    // axios({
    //     method: 'post',
    //     url: `${base}${url}`,
    //     data: params,
    //     transformRequest: [function (data) {
    //         let ret = '';
    //         for (let i in data) {
    //             ret += encodeURIComponent(i) + '=' + encodeURIComponent(data[i]) + '&'
    //         }
    //         return ret;
    //     }],
    //     headers: {
    //         'Content-Type': 'application/x-www-form-urlencoded'
    //     }
    // });
}
export const postRequest = (url, params) => {
    return service.post(
        `${base}${url}`,
        params
    )
    
    // axios({
    //     method: 'post',
    //     url: `${base}${url}`,
    //     data: params
    // })
}
export const putRequest = (url, params) => {
    return service.put(
         `${base}${url}`,
         params
    )
    
    // axios({
    //     method: 'put',
    //     url: `${base}${url}`,
    //     data: params
    // })
}
export const getRequest = (url,param) => {
    console.log('get',service)
    return service.get(`${base}${url}`,param)
}
export const deleteRequest = (url,param) => {
    return service.delete(
        `${base}${url}`,
         param
    )
    // axios({
    //     method: 'delete',
    //     url: `${base}${url}`,
    //     data: param
    // })
}