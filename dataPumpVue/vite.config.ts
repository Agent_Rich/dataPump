import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import topLevelAwait from 'vite-plugin-top-level-await';



// https://vitejs.dev/config/
export default defineConfig({
  base: '/dataPump-api/',
  plugins: [vue(),topLevelAwait()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    port: 5173,
    // proxy: {
    //   // 选项写法
    //   '/dataPump-api': {
    //     target: 'http://127.0.0.1:31433/dataPump-api',
    //     changeOrigin: true,
    //     rewrite: path => path.replace(/^\/dataPump-api/, '')
    //   }
    // }
  }
})
