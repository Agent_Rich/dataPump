# dataPump

#### 介绍
数据泵 用于研发以数据库为源头的模版文本生成器

#### 模版引擎
模版引擎使用Beetle 3 
[模版使用文档](https://www.kancloud.cn/xiandafu/beetl3_guide/1992542)


#### 安装教程
 **注意由于依赖包限制目前仅支持java11及以上版本** 

下载jar包直接启动即可(有java环境变量),完成后点击退出即可。
#### 使用步骤
 **STEP1** 
配置数据源
![configDB](pics/configDB.png)
 **STEP2** 
获取元数据
![metaData](pics/metaData.png)
* copyJson 按钮可以将元数据直接复制到剪贴板，Json格式的数据，可以在编写模版时参考提示。
* packagePath是通用的一个变量可以在模版中使用具体例子可以参考Beetl目录中的模版
* 每一张数据表对应一条数据，tableName旁边的漏斗图标可以筛选。
* option按钮可以配置表内使用的字段,ifID和ifInBaseModel选项可以在模版中当做判读项使用
 **STEP3**
生成文件 
![gen](pics/gen.png)
##### 使用注意
 **模版和输出目录需要以`\`结尾，不然会拼接到文件名中。** 
 **按照模版生成需要配置模版目录，如果只需要用excle，只填写输出目录即可。** 
 **如果文件内容中文乱码，尝试用gbk或者gb2312编码重新打开。** 

#### 更新历史
##### v1.0 
架构替换为Kotlin+Solon+MybatisFlex+Socket.D+vue3+arcoDesign
